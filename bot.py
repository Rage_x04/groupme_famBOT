from flask import Flask
from flask import request
import markovify
import sqlite3
from sqlite3 import Error
import requests
from time import sleep

app = Flask(__name__)


# Rip
# TODO Fix silence

commands = ["output", "train", "trainoutput", "trainonme", "help", "getnummessages", "getnummymessages","setoverlap", "settries", "toggledebug", "toggledb", "silence", "outputsettings"]
training_data = []
markov_model = None
debug_db = "test.db"
release_db = "fam.db"
DB_NAME = None

trained = False
RELEASE = False
isdbrelease = False
db_con = None
db_cursor = None
DEFAULT_TRIES = 200
DEFAULT_OVERLAP = 0.5
silence = False
testbot_id = "e46de10509e9f8a49a9cd60e71"
releasebot_id = "33c4c3a444ebdc9f70e44d0c1d"
BOT_ID = None # current testbot


def create_connection(db_file):
    global db_con
    global db_cursor
    try:
        db_con = sqlite3.connect(db_file)
        print("Sqlite is v{}".format(sqlite3.version))
        db_cursor = db_con.cursor()
        sql_create_data_table = """ CREATE TABLE IF NOT EXISTS groupme_data (
                                        id INTEGER PRIMARY KEY,
                                        sender_id text NOT NULL,
                                        message text
                                    ); """
        db_cursor.execute(sql_create_data_table)

    except Error as e:
        print(e)

def log_msg(sender_id, message):
    global db_cursor
    global db_con
    sql_insert_data = '''INSERT INTO groupme_data(sender_id, message)
                            VALUES(?, ?)'''
    db_cursor.execute(sql_insert_data, (sender_id, message))
    db_con.commit()
    print("Message Logged.")

def get_all_messages():
    global db_cursor
    output = []
    for i in db_cursor.execute("SELECT * FROM groupme_data"):
        output.append(i[2])
    return output


def get_messages_by_sender_id(sender_id):
    global db_cursor
    output = []
    for i in db_cursor.execute("SELECT * FROM groupme_data WHERE sender_id={}".format(sender_id)):
        output.append(i[2])
    return output

def speak(in_message):
    global BOT_ID
    out_data = {"text" : "Your message here", "bot_id" : "e46de10509e9f8a49a9cd60e71"}
    out_data["text"] = in_message
    out_data["bot_id"] = BOT_ID
    url = "https://api.groupme.com/v3/bots/post"
    #print(silence)
    if not silence:
        r = requests.post(url, data=out_data)

def setmode():
    global RELEASE
    global BOT_ID
    global db_con
    if RELEASE:
        print("RELEASE MODE")
        speak("Questions? Run / help")
        DB_NAME = release_db
        BOT_ID = releasebot_id
    else:
        print("DEBUG MODE")
        speak("App running, debug mode.")
        DB_NAME = debug_db
        BOT_ID = testbot_id

    sleep(2)
    db_con.close()
    create_connection(DB_NAME)


def run_command(in_text, sender_id):
    is_command = False
    cur_command = None
    global trained
    global commands
    global markov_model
    global DEFAULT_OVERLAP
    global DEFAULT_TRIES
    for i in commands:
        if "/{}".format(i) in in_text:
            print("Command is: {}".format(i))
            cur_command = i
            is_command = True
    if not is_command:
        log_msg(sender_id, in_text)

    if cur_command == "train":
        trained = True
        full_text_string = ""
        for i in get_all_messages():
            full_text_string = full_text_string + i + "\n"
        print("Training data is:\n\n {}".format(full_text_string))
        markov_model = markovify.NewlineText(full_text_string)
        speak("Markov created.")
    elif cur_command == "output":
        
        if not trained:
            speak("Markov must first be trained")
        else:
            # Something is up here
            output = markov_model.make_short_sentence(140, tries=DEFAULT_TRIES, max_overlap_ratio=DEFAULT_OVERLAP)
            if output == None:
                speak("Either tries are set too low, or overlap ratio is set too high, or I do not have enough data.")
                print("Either tries are set too low, or overlap ratio is set too high, or I do not have enough data.")
            else:
                speak(output)
                print("Output is: {}".format(output))
    elif cur_command == "trainonme":
        trained = True
        full_text_string = ""
        for i in get_messages_by_sender_id(sender_id):
            full_text_string = full_text_string + i + "\n"
        print("Training data from user {} is:\n\n {}".format(sender_id, full_text_string))
        markov_model = markovify.NewlineText(full_text_string)
        speak("Markov created.")
    elif cur_command == "help":
        output = '''
        /help               print this message
        /train              train markov chain on all messages
        /trainonme          tain markov chain on my messages
        /output             output the last version of the markov chain
        /getnummessages     prints total number of messages logged
        /settries [num]     sets how many times the markov chain will try to make a unique sentence default: 200
        /setoverlap [float] sets the percentage of overlap in produced sentence vs trained default: 0.5
        /silence            Toggles the bots ability to speak in chat default: false
        /toggledebug        prints debug info, and switches chat channel
        /toggledb           switches db from debug to release
        /getnummymessages   outputs logged number of messages for calling user
        /outputsettings     outputs current settings
        /trainoutput        trains and outputs
        '''
        speak(output)
        print("Trainonme output: {}".format(output))
    elif cur_command == "getnummessages":
        num = len(get_all_messages())
        speak("Total messages logged: {}".format(num))
        print("Total messages logged: {}".format(num))
    elif cur_command == "settries":
        tries = in_text.split(" ")[1]
        DEFAULT_TRIES = int(tries)
        print(DEFAULT_TRIES)
    elif cur_command == "setoverlap":
        overlap = in_text.split(" ")[1]
        DEFAULT_OVERLAP = float(overlap)
        print(DEFAULT_OVERLAP)
    elif cur_command == "silence":
        global silence
        silence = not silence
        print("Silence is {}".format(silence))
        if not silence:
            print("can speak again")
        else:
            print("silenced")
    elif cur_command == "toggledebug":
        global RELEASE
        RELEASE = not RELEASE
        if RELEASE:
            speak("Current mode: release")
            print("Current mode: release")
        else:
            speak("Current mode: debug")
            print("Current mode: debug")
        setmode()
    elif cur_command == "toggledb":
        global db_con
        global isdbrelease
        global DB_NAME
        isdbrelease = not isdbrelease
        if isdbrelease:
            DB_NAME = release_db
        else:
            DB_NAME = debug_db
        db_con.close()
        create_connection(DB_NAME)
        speak("Now connected to {}".format(DB_NAME))
        print("Now connected to {}".format(DB_NAME))
    elif cur_command == "getnummymessages":
        speak("You have {} messages logged".format(len(get_messages_by_sender_id(sender_id))))
        print("You have {} messages logged".format(len(get_messages_by_sender_id(sender_id))))
    elif cur_command == "outputsettings":
        output = ''' 
        Tries: {}\n
        Overlap: {}\n
        Db: {}\n
        Release Mode: {}\n
        Silenced: {}\n
        '''.format(DEFAULT_TRIES, DEFAULT_OVERLAP, DB_NAME, RELEASE, silence)
        speak(output)
        print(output)
    elif cur_command == "trainoutput":
        run_command("/silence", sender_id)
        run_command("/train", sender_id)
        run_command("/silence", sender_id)
        run_command("/output", sender_id)
    elif cur_command == None:
        pass



@app.route("/", methods=['GET', 'POST'])
def hello():
    global db_cursor
    if request.method == "POST":
        x = request.get_json()
        message = x['text']
        if x['sender_id'] == '593734' or x['sender_id'] == '593763':
            print("FOUND MY MESSAGE!!")
        else:
            run_command(message, x['sender_id'])

        
        
    return "Hello World!"

if __name__ == "__main__":
    if RELEASE:
        print("RELEASE MODE")
        speak("Questions? Run / help")
        DB_NAME = release_db
        BOT_ID = releasebot_id
    else:
        print("DEBUG MODE")
        speak("App running, debug mode.")
        DB_NAME = debug_db
        BOT_ID = testbot_id

    create_connection(DB_NAME)
    app.run(host='0.0.0.0', port=6969)


