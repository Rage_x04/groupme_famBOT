from flask import Flask
from flask import request
import pickle

app = Flask(__name__)

file_save_dir = "test_events/"
num_request = 0
template = "request{}.pkl"


def saveJSON(json):
    global num_request
    filename = template.format(num_request)
    outpath = file_save_dir + filename
    with open(outpath, "wb") as f:
        pickle.dump(json, f)
    print("saved to: {}".format(outpath))

def loadJSON(file_index):
    output = None
    filename = template.format(file_index)
    outpath = file_save_dir + filename
    with open(outpath, 'rb') as f:
        ouput = pickle.load(f)
    return output

@app.route("/", methods=['GET', 'POST'])
def index():
    global num_request
    print("got request {}".format(num_request))
    x = request.get_json()
    print(x)
    saveJSON(x)
    num_request += 1
    return "U shouldnt b here"

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=6969)
