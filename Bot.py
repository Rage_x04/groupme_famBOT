import markovify
import sqlite3
import datetime
from sqlite3 import Error
import requests
import json


class groupme_message():
    POST_URL = "https://api.groupme.com/v3/bots/post"
    VERBOSE = False

    def __init__(self, input_json=None):
        if input_json != None:
            self.data = input_json
            self.logged_message = True
        else:
            self.logged_message = False
            default_mbody = {
                "bot_id": None,
                "text": None,
                "attachments": []
            }
            self.data = default_mbody

    def set_message(self, msg):
        self.data["text"] = msg

    def set_message_id(self, mid):
        self.id = mid

    def set_bot_id(self, bot_id):
        self.data["bot_id"] = bot_id

    def add_mention(self, userids, loci):
        #Both userids & loci are lists
        # Userids [uuuu1, uuuuu2, uuuu3, ...]
        # Loci is [[startindex, length], [0, 6], ...]
        #           startindex as in the index of which the mention begins in "text"

        mention_dict = {
            "type": "mentions",
            "user_ids": userids,
            "loci": loci
        }
        self.data["attachments"].append(mention_dict)

    def validate_parts(self):
        validated = False
        if self.data["bot_id"] != None:
            if self.data["text"] != None:
                validated = True
        return validated

    def gen_message(self, botid, message):
        self.set_message(message)
        self.set_bot_id(botid)

    def send_message(self):
        can_send = self.validate_parts()
        if can_send:
            r = requests.post(groupme_message.POST_URL, data=json.dumps(self.data))
        else:
            print("MESSAGE VALIDATION ERROR")

    def get_text(self):
        return self.data["text"]

    def get_message_id(self):
        return self.id

    def get_single_message(self):
        url = "https://api.groupme.com/v3/groups/29384054/messages?token=lNq4ftToJbIcXuiNlZhzVTCJMWZxRbDm2so9PIvW&limit=1"
        # Token is needed for auth, or, the token provided in a header
        # limit is how many messages to grab, from 1-100, returns json to parse
        pass



# Use a technique similar to tweepy/models/User
# This will set all kv pairs in json to attributes, then we can inspect what they are
# Using setattr()
# Also have a message_json to store the raw json



class DbManager:
    # DEFAULT OPTIONS /\/\//\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/
    DATABASE = "debug"
    Cursor = None
    Connection = None
    Connected = False
    VERBOSE = False  # Used to output debug info

    #  STATIC OPTIONS \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
    db_files = {"debug": "test1.db", "release": "fam1.db"}
    db_tables = {
        "groupme_data": ["id INTEGER PRIMARY KEY", "sender_id  NOT NULL", "message TEXT", "datetime TEXT"],
        "users": ["sender_id TEXT PRIMARY KEY", "username TEXT NOT NULL"],
        "bot_output": ["message_id TEXT PRIMARY KEY", "datetime TEXT NOT NULL", "output TEXT NOT NULL", "likes INTEGER NOT NULL", "initiator_id TEXT NOT NULL"]
                 }

    @classmethod
    def connect(cls, type=None):
        # connect() autoconnects to debug
        # connect("release") connects to release
        # connect("debug") connects to debug
        #   VERBOSE outputs the connected db and sqlite verson on connect
        if DbManager.Connected == False and type is None:
            DbManager.Connection = sqlite3.connect(DbManager.db_files[DbManager.DATABASE])
            DbManager.Connected = True
            DbManager.Cursor = DbManager.Connection.cursor()
        elif type is not None:
            if type == "release":
                DbManager.DATABASE = "release"
                DbManager.Connection = sqlite3.connect(DbManager.db_files[DbManager.DATABASE])
                DbManager.Cursor = DbManager.Connection.cursor()
                DbManager.Connected = True
            elif type == "debug":
                DbManager.DATABASE = "debug"
                DbManager.Connection = sqlite3.connect(DbManager.db_files[DbManager.DATABASE])
                DbManager.Cursor = DbManager.Connection.cursor()
                DbManager.Connected = True
            else:
                class DbTypeException(Exception):
                    def __init__(self, message):
                        self.message = "Incorrect DbType in connect().\nType should be either \"release\" or \"debug\""
                raise DbTypeException

            if DbManager.VERBOSE:
                print("Sqlite version is {}".format(sqlite3.version))
                print("Db connected to: {}".format(DbManager.db_files[DbManager.DATABASE]))

    @classmethod
    def toggleDb(cls):
        # toggleDb() swaps the currently connected Db
        #           from release to debug and visa-versa
        # Verbose outputs the connected DB after the toggle
        if DbManager.DATABASE == "debug":
            DbManager.connect("release")
        elif DbManager.DATABASE == "release":
            DbManager.connect("debug")

        if DbManager.VERBOSE:
            print("Now connected to {}.".format(DbManager.db_files[DbManager.DATABASE]))

    @classmethod
    def toggle_verbose(cls):
        # Toggles VERBOSE, obviously
        DbManager.VERBOSE = not DbManager.VERBOSE

    @classmethod
    def create_tables(cls):
        for key in DbManager.db_tables.keys():
            command = '''CREATE TABLE IF NOT EXISTS {} ('''.format(key)
            for column in DbManager.db_tables[key]:
                command += '''{}'''.format(column)
                if DbManager.db_tables[key].index(column) < len(DbManager.db_tables[key]) -1:
                    command += ''', '''
            command += ''');'''
            if DbManager.VERBOSE:
                print(command)
            DbManager.Cursor.execute(command)

    @classmethod
    def log_msg(cls, sender_id, message):
        # Logs a basic groupme message
        command = '''INSERT INTO groupme_data(sender_id, message, datetime)
                        VALUES (?, ?, ?)'''
        datetime2save = datetime.datetime.now()
        DbManager.Cursor.execute(command, (sender_id, message, datetime2save))
        DbManager.Connection.commit()

    @classmethod
    def log_output(cls, message_id, initiator_id, output):
        # Logs bot output
        command = '''INSERT INTO bot_output(message_id, datetime, output, likes, initiator_id)
                        VALUES (?, ?, ?, ?, ?)'''
        datetime2save = datetime.datetime.now()
        likes = 0
        DbManager.Cursor.execute(command, (message_id, datetime2save, output, likes, initiator_id))
        DbManager.Connection.commit()

    @classmethod
    def output_add_one_like(cls, message_id):
        # Takes likes = likes + 1 where message_id in bot_output
        try:
            command_update = '''UPDATE bot_output
                                    SET likes = ?
                                    WHERE message_id = ?; '''
            command_get_current = '''SELECT likes FROM bot_output
                                        WHERE message_id = ?; '''

            DbManager.Cursor.execute(command_get_current, (message_id,)) # Comma here is necessary
            current_likes = DbManager.Cursor.fetchone()[0]

            current_likes += 1
            DbManager.Cursor.execute(command_update, (current_likes, message_id))
            DbManager.Connection.commit()
        except TypeError:
            print("Error: message_id = {} not found. Is it valid?".format(message_id))

    # UNTESTED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @classmethod
    def log_user(cls, sender_id, username):
        # Adds a user into the user Db
        command = '''INSERT INTO users(sender_id, username)
                            VALUES (?, ?);'''
        try:
            DbManager.Cursor.execute(command, (sender_id, username))
            DbManager.Connection.commit()
        except sqlite3.IntegrityError:
            if DbManager.get_user_by_id(sender_id) != username:
                if DbManager.VERBOSE:
                    print("{} changed username to {}".format(DbManager.get_user_by_id(sender_id), username))
                DbManager.update_username(sender_id, username) # Update the username, if the one in the Db and the one on the message are different

            if DbManager.VERBOSE:
                print("User: {} already logged. skipping.....".format(username))

    @classmethod
    def get_user_by_id(cls, sender_id):
        # Returns the username of the user who holds the specified ID
        command = '''SELECT username FROM users
                        WHERE sender_id = ?;'''

        DbManager.Cursor.execute(command, (sender_id,))
        output = DbManager.Cursor.fetchone()[0]
        return output

    @classmethod
    def get_id_by_username(cls, username):
        # Returns the ID of the user that holds the specified username
        command = '''SELECT sender_id FROM users
                        WHERE username = ?;'''
        DbManager.Cursor.execute(command, (username,))
        output = DbManager.Cursor.fetchone()[0]
        return output

    @classmethod
    def update_username(cls, sender_id, new_username):
        command = '''UPDATE users
                        SET username = ?
                        WHERE sender_id = ?'''
        DbManager.Cursor.execute(command, (new_username, sender_id))
        DbManager.Connection.commit()

# ALL ABOVE UNTESTED !!!!!!!!!!!!!!!!!!!!!!!BEWARE!!!!!!!!!!!!!!!


class Bot:
    # DEFAULT OPTIONS /\/\/\/\/\/\/\/\/\/\/\/\/
    TRIES = 200
    OVERLAP = 0.5
    SILENCE = False
    DEBUG = True
    BOT_TYPE = "debug"
    MARKOV_MODEL = None


    # STATIC VALUES /\/\/\/\/\/\/\/\/\/\/\/\/\/
    BOT_IDS = {"release": "33c4c3a444ebdc9f70e44d0c1d", "debug": "e46de10509e9f8a49a9cd60e71"}
    commands = [
                "output", "train", "trainoutput", "trainonme",
                "help", "getnummessages", "getnummymessages",
                "setoverlap", "settries", "toggledebug", "toggledb",
                "silence", "outputsettings"
    ]

    training_data = []

    pass


if __name__ == "__main__":
    DbManager.connect()
    DbManager.create_tables()
    DbManager.toggle_verbose()
    DbManager.log_msg("uuid1234567", "twat da fook")
    #DbManager.log_output("LolTestID", "Fooker", "Whelp. This is a test")
    #DbManager.output_add_one_like("LolTesetID")
    DbManager.log_user("23456789", "ripriprip")
    DbManager.log_user("23456789", "tehehehehe")
    x = groupme_message()
    x.gen_message(Bot.BOT_IDS["debug"], "test")
    #x.add_mention([21499516], [[0, 3]])
    print(x.data)
    groupme_message.VERBOSE = True
    x.send_message()



